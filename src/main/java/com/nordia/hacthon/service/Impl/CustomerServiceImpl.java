package com.nordia.hacthon.service.Impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.nordia.hacthon.dto.ApiResponse;
import com.nordia.hacthon.dto.CustomerDto;
import com.nordia.hacthon.entity.Customer;
import com.nordia.hacthon.exception.ResourceConflictExists;
import com.nordia.hacthon.repository.CustomerRepository;
import com.nordia.hacthon.service.CustomerService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;

	@Override
	public ApiResponse register(CustomerDto customerDto) {

		Optional<Customer> customer = customerRepository
				.findByFirstNameIgnoreCaseAndAddressIgnoreCase(customerDto.getFirstName(), customerDto.getAddress());

		if (customer.isPresent()) {
			throw new ResourceConflictExists("Customer already Register in this address ");
		}
		Customer customer1 = Customer.builder().firstName(customerDto.getFirstName())
				.lastName(customerDto.getLastName()).address(customerDto.getAddress()).gender(customerDto.getGender())
				.build();
		customerRepository.save(customer1);

		return ApiResponse.builder().code(201l).message("Customer register succefully").build();
	}

}