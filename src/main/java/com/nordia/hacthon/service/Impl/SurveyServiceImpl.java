package com.nordia.hacthon.service.Impl;

import java.io.FileWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nordia.hacthon.entity.SurveyDetails;
import com.nordia.hacthon.repository.ServeyDtailsRepository;
import com.nordia.hacthon.service.SurveyService;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
@Service
public class SurveyServiceImpl implements SurveyService{
	@Autowired
    private ServeyDtailsRepository repository;

    @Override
    public List<SurveyDetails> getAllEntities() {
        return repository.findAll();
    }

    @Override
    public void generateCSV() {
        List<SurveyDetails> entities = getAllEntities();
        try (Writer writer = new FileWriter("bank.csv")) {
            Field[] fields = SurveyDetails.class.getDeclaredFields();

            // Write headers

            writer.write(Arrays.stream(fields)

                    .map(Field::getName)

                    .collect(Collectors.joining(",")) + "\n");
            StatefulBeanToCsv<SurveyDetails> beanToCsv = new StatefulBeanToCsvBuilder<SurveyDetails>(writer)
                    .withSeparator(',')
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                    .build();
            beanToCsv.write(entities);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    


}
