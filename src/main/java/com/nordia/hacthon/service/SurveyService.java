package com.nordia.hacthon.service;

import java.util.List;

import com.nordia.hacthon.entity.SurveyDetails;

public interface SurveyService {
    List<SurveyDetails> getAllEntities();
    void generateCSV();

}
