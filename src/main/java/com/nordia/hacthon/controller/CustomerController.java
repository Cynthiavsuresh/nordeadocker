package com.nordia.hacthon.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nordia.hacthon.dto.ApiResponse;
import com.nordia.hacthon.dto.CustomerDto;
import com.nordia.hacthon.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CustomerController {
	
	private final CustomerService customerService;
	@PostMapping("/customer")
	public ResponseEntity<ApiResponse> register(@Valid @RequestBody CustomerDto customerDto){
		return new ResponseEntity<>(customerService.register(customerDto),HttpStatus.CREATED);
	}
	

}
