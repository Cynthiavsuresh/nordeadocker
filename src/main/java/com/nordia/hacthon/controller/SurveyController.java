package com.nordia.hacthon.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nordia.hacthon.exception.EntityFetchingException;
import com.nordia.hacthon.service.SurveyService;

import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class SurveyController {
	@Autowired
    private SurveyService service;

	@PostMapping("/entities/csv")
	   public ResponseEntity<?> generateCSV(HttpServletResponse response) throws EntityFetchingException {
        try {
            service.generateCSV();
            // Set response headers
            response.setContentType("text/csv");
            response.setHeader("Content-Disposition", "attachment; filename=bank.csv");
            // Write file content to response
            Files.copy(Paths.get("bank.csv"), response.getOutputStream());
            response.getOutputStream().flush();
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to generate CSV");
        }
    }
}
