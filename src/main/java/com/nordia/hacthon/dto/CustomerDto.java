package com.nordia.hacthon.dto;

import com.nordia.hacthon.entity.Gender;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerDto {
	@NotBlank(message = "firstName required")
	private String firstName;
	private String lastName;
	@NotBlank(message = "address required")
	private String address;
	private Gender gender;}