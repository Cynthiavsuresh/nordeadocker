package com.nordia.hacthon.exception;



import org.springframework.http.HttpStatus;

public class GlobalErrorCode {
    public static final HttpStatus ERROR_RESOURCE_NOT_FOUND = HttpStatus.NOT_FOUND;
    public static final HttpStatus ERROR_RESOURCE_CONFLICT_EXISTS = HttpStatus.CONFLICT;
    public static final HttpStatus ERROR_UNAUTHOURIZED_USER=HttpStatus.UNAUTHORIZED;
    public static final HttpStatus INTERNAL_SERVER_ERROR=HttpStatus.INTERNAL_SERVER_ERROR;
    
    public GlobalErrorCode() {
		super();
	}
}
