package com.nordia.hacthon.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nordia.hacthon.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
	
	Optional<Customer> findByFirstNameIgnoreCaseAndAddressIgnoreCase(String firstName, String address);

}

