package com.nordia.hacthon.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nordia.hacthon.entity.SurveyDetails;

public interface ServeyDtailsRepository extends JpaRepository<SurveyDetails, Integer> {

}
