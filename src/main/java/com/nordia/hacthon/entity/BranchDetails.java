package com.nordia.hacthon.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BranchDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int BranchID;
	private String branchName;
	private String branchAddress;}