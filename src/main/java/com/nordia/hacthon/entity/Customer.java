package com.nordia.hacthon.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customId;
	private String firstName;
	private String lastName;
	private String address;
	@Enumerated(EnumType.STRING)
	private Gender gender;
	@OneToOne
	private BranchDetails branch;

}
