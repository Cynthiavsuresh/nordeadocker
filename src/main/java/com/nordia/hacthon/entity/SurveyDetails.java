package com.nordia.hacthon.entity;

import com.opencsv.bean.CsvBindByPosition;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SurveyDetails {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@CsvBindByPosition(position = 0)
	int SurveyID;
	@CsvBindByPosition(position = 1)
	int CustomerID ;
	@CsvBindByPosition(position = 2)
	int BranchID;
	@CsvBindByPosition(position = 3)
	double Distance;
}
