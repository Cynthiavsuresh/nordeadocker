package com.example.demo.service.Impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.nordia.hacthon.dto.ApiResponse;
import com.nordia.hacthon.dto.CustomerDto;
import com.nordia.hacthon.entity.Customer;
import com.nordia.hacthon.exception.ResourceConflictExists;
import com.nordia.hacthon.repository.CustomerRepository;
import com.nordia.hacthon.service.Impl.CustomerServiceImpl;



@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {
	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;
	@Mock
	private CustomerRepository customerRepository;

	@Test
	void customerRegisterSuccefull() {
		Customer customer = Customer.builder().firstName("Praveen").address("Bangalore").build();
		Mockito.when(customerRepository.findByFirstNameIgnoreCaseAndAddressIgnoreCase("Praveen", "Bangalore"))
				.thenReturn(Optional.empty());
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		CustomerDto customerDto = CustomerDto.builder().firstName("Praveen").address("Bangalore").build();
		ApiResponse register = customerServiceImpl.register(customerDto);
		assertEquals("Customer register succefully", register.getMessage());
	}

	@Test
	void customerRegisterUnSucce() {
		Customer customer = Customer.builder().firstName("Praveen").address("Bangalore").build();
		Mockito.when(customerRepository.findByFirstNameIgnoreCaseAndAddressIgnoreCase("Praveen", "Bangalore"))
				.thenReturn(Optional.of(customer));
		CustomerDto customerDto = CustomerDto.builder().firstName("Praveen").address("Bangalore").build();
		assertThrows(ResourceConflictExists.class, () -> customerServiceImpl.register(customerDto));
	}

}