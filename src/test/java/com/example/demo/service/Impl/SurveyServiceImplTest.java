package com.example.demo.service.Impl;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nordia.hacthon.entity.SurveyDetails;
import com.nordia.hacthon.repository.ServeyDtailsRepository;
import com.nordia.hacthon.service.Impl.SurveyServiceImpl;
 
@ExtendWith(MockitoExtension.class)
public class SurveyServiceImplTest {
 
    @Mock
    private ServeyDtailsRepository repositoryMock;
 
    @InjectMocks
    private SurveyServiceImpl service;
 
    @Test
    public void testGenerateCSV() throws Exception {
        // Mock entity data
        List<SurveyDetails> mockEntities = Arrays.asList(
                SurveyDetails.builder().SurveyID(1).CustomerID(101).BranchID(201).Distance(10.5).build(),
                SurveyDetails.builder().SurveyID(2).CustomerID(102).BranchID(202).Distance(20.5).build()
        );
 
        // Mock repository behavior
        when(repositoryMock.findAll()).thenReturn(mockEntities);
 
        // Call the method to test
        service.generateCSV();
 
        // Verify that findAll method of repository is called
        verify(repositoryMock).findAll();
 
        // You can also further assert the behavior as needed
    }
 
//    @Test
//    public void testGenerateCSV_NullEntities() {
//        // Mock repository to return null
//        when(repositoryMock.findAll()).thenReturn(null);
// 
//        // Call the method to test
//        assertThrows(EntityFetchingException.class, () -> service.generateCSV());
//    }
// 
//    @Test
//    public void testGenerateCSV_ExceptionDuringCSVGeneration() {
//        // Mock repository behavior
//        when(repositoryMock.findAll()).thenReturn(Arrays.asList(new SurveyDetails()));
// 
//        // Mock FileWriter to throw an exception
//        doThrow(IOException.class).when(service).generateCSV();
// 
//        // Call the method to test
//        assertThrows(IOException.class, () -> service.generateCSV());
//    }
}